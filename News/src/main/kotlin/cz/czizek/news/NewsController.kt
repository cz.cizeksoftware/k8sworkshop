package cz.czizek.news

import com.hazelcast.core.HazelcastInstance
import org.springframework.beans.factory.annotation.Value
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
class NewsController(val hazelcastInstance: HazelcastInstance) {

    @Value("\${HOSTNAME}")
    lateinit var hostname: String

    val news: MutableList<String> get() = hazelcastInstance.getList("news")

    @GetMapping("add")
    fun addNews(@RequestParam news: String) {
        this.news.add("$news ($hostname)")
    }

    @GetMapping
    fun news(model: Model): String {

        return news.joinToString(separator = "\n<br/>") { it }
    }
}