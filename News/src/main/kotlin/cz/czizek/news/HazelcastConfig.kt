package cz.czizek.news

import com.hazelcast.config.Config
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class HazelcastConfig {
    @Value("\${HOSTNAME}")
    lateinit var hostname: String

    @Bean
    fun hazelcast(): Config {
        val config = Config()
        config.setInstanceName("$hostname-hazelcast-instance")

        return config
    }
}