package cz.czizek.timeteller

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalTime
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField

fun LocalTime.formatted(): String = this.format(DateTimeFormatterBuilder()
        .appendValue(ChronoField.HOUR_OF_DAY, 2)
        .appendLiteral(':')
        .appendValue(ChronoField.MINUTE_OF_HOUR, 2)
        .optionalStart()
        .appendLiteral(':')
        .appendValue(ChronoField.SECOND_OF_MINUTE, 2)
        .toFormatter())

@RestController
@RequestMapping("/")
class TimeTellingController {


    @Value("\${HOSTNAME}")
    lateinit var hostname: String

    @GetMapping
    fun tellTime(): String {
        return "Current time is ${LocalTime.now().formatted()} ($hostname told you that)"
    }
}