package cz.czizek.timeteller

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TimetellerApplication

fun main(args: Array<String>) {
    runApplication<TimetellerApplication>(*args)
}
