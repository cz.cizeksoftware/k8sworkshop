package cz.czizek.hellofromdocker

import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.IList
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.runApplication
import org.springframework.context.event.EventListener

@SpringBootApplication
class HellofromdockerApplication : DisposableBean {

    @Value("\${HOSTNAME}")
    lateinit var hostname: String

    @Autowired
    lateinit var hazelcastInstance: HazelcastInstance

    @EventListener(ApplicationStartedEvent::class)
    fun onApplicationEvent() {
        hazelcastInstance.instances.add(hostname)
    }

    override fun destroy() {
        hazelcastInstance.instances.remove(hostname)
    }
}

val HazelcastInstance.instances: IList<String> get() = this.getList("instances")

fun main(args: Array<String>) {
    runApplication<HellofromdockerApplication>(*args)
}
