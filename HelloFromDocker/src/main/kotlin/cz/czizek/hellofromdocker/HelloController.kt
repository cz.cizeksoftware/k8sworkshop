package cz.czizek.hellofromdocker

import com.hazelcast.core.HazelcastInstance
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.net.URL

@Controller
@RequestMapping("/")
class HelloController(val hazelcastInstance: HazelcastInstance) {

    @Value("\${HOSTNAME}")
    lateinit var hostname: String

    @Value("\${timeteller.url}")
    lateinit var timetellerUrl: String

    @Value("\${news.url}")
    lateinit var newsUrl: String

    @Value("\${environment-specific.value}")
    lateinit var environmentSpecificValue: String

    @GetMapping
    fun hello(model: Model): String {

        val timeTellerMessage = urlTextOrExceptionMessage(timetellerUrl)
        val news = urlTextOrExceptionMessage(newsUrl)

        val hazelcastMembers = hazelcastInstance.cluster.members.size
        model.addAttribute("hazelcastMembers", hazelcastMembers)
        model.addAttribute("hostname", hostname)
        model.addAttribute("instances", hazelcastInstance.instances)

        model.addAttribute("timetellerUrl", timetellerUrl)
        model.addAttribute("timetellerMessage", timeTellerMessage)

        model.addAttribute("environmentSpecificValue", environmentSpecificValue)

        model.addAttribute("newsUrl", newsUrl)
        model.addAttribute("news", news)

        return "hello"
    }

    private fun urlTextOrExceptionMessage(url: String): String {
        return try {
            URL(url).readText()
        } catch (e: Exception) {
            e.message
        }!!
    }
}